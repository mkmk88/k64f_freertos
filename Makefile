TARGET=k64f
CC=arm-none-eabi-gcc
OBJCPY=arm-none-eabi-objcopy
CFLAGS=-Wall -Wextra -mfloat-abi=softfp -mthumb -mcpu=cortex-m4 -g -nostartfiles
LDSCRIPT=linker/k64f.ld
SRCS=\
    startup/startup.s \
    startup/startup.c \
    portable/GCC/ARM_CM4F/port.c \
    portable/MemMang/heap_1.c \
    event_groups.c \
    list.c \
    queue.c \
    tasks.c \
    timers.c \
    app/main.c
INCLUDES=\
    -Iinclude \
    -Iconfig \
    -Iportable/GCC/ARM_CM4F \

all:
	$(CC) $(INCLUDES) $(SRCS) $(CFLAGS) -T $(LDSCRIPT) -o $(TARGET).elf
	$(OBJCPY) k64f.elf k64f.bin -O binary

clean:
	rm $(TARGET).elf $(TARGET).bin
