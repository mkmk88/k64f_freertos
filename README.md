# README #

This is a basic GCC setup for Freescale Freedom K64F board (http://www.freescale.com/products/arm-processors/kinetis-cortex-m/k-series/k6x-ethernet-mcus/freescale-freedom-development-platform-for-kinetis-k64-k63-and-k24-mcus:FRDM-K64F) to run freeRTOS (http://www.freertos.org/). 

NOTE: The functionality is very limited - *there are no interrupts in vector table other than those needed by freeRTOS*. This SW doesn't use CMSIS, however it shouldn't be a problem to include it. More advanced stuff is ongoing on the 'lwip' branch.

The only thing the startup code does is disable watchdog and initialize .data and .bss sections. In this version vectors aren't copied to RAM.

To use malloc() (specifically newlib's malloc_r) based heap you need to implement sbrk(). Simple example can be found here: https://bitbucket.org/mkmk88/frdm-k64f_minimal (see branch "stdlib_no_crt0").

### How do I get set up? ###

* Clone this repository.
* Get ARM GCC toolchain https://launchpad.net/gcc-arm-embedded.
* Add ARM GCC toolchain to path.
* Go to repo's root directory and run "make". Two files should be generated: k64f.elf and k64f.bin.
* Copy k64f.bin to the board. Blue LED should start blinking.
* (lwip branch): To ping over IPv4 ping 192.168.1.11
* (lwip branch): To ping over IPv6 ping6 fe80::2cf:52ff:fe35:1

### How to debug? ###

* You can use pyOCD (https://github.com/mbedmicro/pyOCD) and gdb to debug.
* Run pyOCD: sudo pyocd-gdbserver.
* On the other console run gdb: arm-none-eabi-gdb k64f.elf.
* In gdb issue following commands: "target remote localhost:3333", "load", "monitor reset halt".
* You can now debug.
 
### Who do I talk to? ###

* Michal Kowalczyk kowalczykmichal88+bb@gmail.com
* http://fixbugfix.blogspot.com/