.cpu cortex-m4
.thumb

.section .vectors, "a"
    .word __stack
    .word _reset
    .word _hang /* NMI */
    .word _hang /* HardFault */
    .word _hang /* MemManage */
    .word _hang /* BusFault */
    .word _hang /* UsageFault */
    .word _hang /* Reserved */
    .word _hang /* Reserved */
    .word _hang /* Reserved */
    .word _hang /* Reserved */
    .word vPortSVCHandler
    .word _hang /* DebugMonitor */
    .word _hang /* Reserved */
    .word xPortPendSVHandler
    .word xPortSysTickHandler

.section .flash_config, "a"
    .long 0xFFFFFFFF
    .long 0xFFFFFFFF
    .long 0xFFFFFFFF
    .long 0xFFFFFFFE

.section .text
.thumb_func
.global _reset
_reset:
    bl disable_wdog
    bl init_data_and_bss
    bl main

.global _exit
_exit:
    b .
 
.global _hang
_hang:
    b .
