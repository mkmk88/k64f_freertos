#define WDOG_STCTRLH (*(short *)0x40052000u)
#define WDOG_UNLOCK (*(short  *)0x4005200Eu)

#define WDOG_UNLOCK_WDOGUNLOCK_MASK 0xFFFFu
#define WDOG_UNLOCK_WDOGUNLOCK_SHIFT 0
#define WDOG_UNLOCK_WDOGUNLOCK_WIDTH 16
#define WDOG_UNLOCK_WDOGUNLOCK(x) (((short)(((short)(x))<<WDOG_UNLOCK_WDOGUNLOCK_SHIFT))&WDOG_UNLOCK_WDOGUNLOCK_MASK)

#define WDOG_STCTRLH_WAITEN_MASK 0x80u
#define WDOG_STCTRLH_STOPEN_MASK 0x40u
#define WDOG_STCTRLH_ALLOWUPDATE_MASK 0x10u
#define WDOG_STCTRLH_CLKSRC_MASK 0x2u

#define WDOG_STCTRLH_BYTESEL_MASK 0x3000u
#define WDOG_STCTRLH_BYTESEL_SHIFT 12
#define WDOG_STCTRLH_BYTESEL(x) (((short)(((short)(x))<<WDOG_STCTRLH_BYTESEL_SHIFT))&WDOG_STCTRLH_BYTESEL_MASK)

extern unsigned int _sfdata;
extern unsigned int _edata;
extern unsigned int _sdata;
extern unsigned int __bss_start__;
extern unsigned int __bss_end__;

void disable_wdog()
{
    WDOG_UNLOCK = WDOG_UNLOCK_WDOGUNLOCK(0xC520);
    WDOG_UNLOCK = WDOG_UNLOCK_WDOGUNLOCK(0xD928);
    WDOG_STCTRLH = WDOG_STCTRLH_BYTESEL(0x00) |
        WDOG_STCTRLH_WAITEN_MASK |
        WDOG_STCTRLH_STOPEN_MASK |
        WDOG_STCTRLH_ALLOWUPDATE_MASK |
        WDOG_STCTRLH_CLKSRC_MASK |
        0x0100U;
}

void init_data_and_bss()
{
    unsigned int *src, *dst;

    src = &_sfdata;

    for(dst = &_sdata; dst < &_edata;)
    {
        *(dst++) = *(src++);
    }

    for(src = &__bss_start__; src < &__bss_end__;)
    {
        *(src++) = 0;
    }
}
