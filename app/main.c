#include "FreeRTOS.h"
#include "task.h"

#define SIM_SCGC5 (*(int *)0x40048038u)
#define SIM_SCGC5_PORTB 10

#define PORTB_PCR21 (*(int *)0x4004A054u)
#define PORTB_PCR21_MUX 8

#define GPIOB_PDDR (*(int *)0x400FF054u)
#define GPIOB_PDOR (*(int *)0x400FF040u)
#define PIN_N 21

void init_gpio()
{
    /* Enable clocks. */
    SIM_SCGC5 |= 1 << SIM_SCGC5_PORTB;
    /* Configure pin 21 as GPIO. */
    PORTB_PCR21 |= 1 << PORTB_PCR21_MUX;
    /* Configure GPIO pin 21 as output.
     * It will have a default output value set
     * to 0, so LED will light (negative logic).
     */
    GPIOB_PDDR |= 1 << PIN_N;
}

void blinky_task()
{
    while(1) {
        GPIOB_PDOR ^= 1 << PIN_N;
        vTaskDelay(500 / portTICK_PERIOD_MS);
    }
}

int main()
{
    init_gpio();

    xTaskCreate(blinky_task, "Blinky", 100, NULL, 1, NULL);
    vTaskStartScheduler();
    return 0;
}
